use serde::{Serialize, Deserialize, Serializer, Deserializer, de::{self, DeserializeOwned, Visitor, SeqAccess, MapAccess, Unexpected}, ser::SerializeStruct};
use std::{str::FromStr, fmt::{self, Debug, Display}, marker::PhantomData, collections::{BTreeMap, BTreeSet}};
use strct_http::message::{request::Method, Message};

pub struct Resource<T: Identifiable> {
    storage: Storage<T>,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Link<T: Identifiable> {
    id: <T as Identifiable>::Id,
}

pub struct Storage<T: Identifiable> {
    origin: BTreeMap<<T as Identifiable>::Id, T>,
}

pub trait Identifiable: Debug + Ord + Serialize {
    type Id: Debug + Display + Ord + FromStr + Serialize + DeserializeOwned;

    fn identity() -> &'static str;

    fn id(&self) -> Self::Id;
}

impl<T: Identifiable> Link<T> {
    pub fn new(id: <T as Identifiable>::Id) -> Self {
        Link {
            id
        }
    }
}

impl<T: Identifiable> Resource<T> {
    pub fn new() -> Self {
        Resource {
            storage: Storage::new(),
        }
    }

    //TODO: instead of the whole enum this method should support only Request variant?
    pub fn process(request: Message) -> Result<Message, &'static str> {
        match request {
            Message::Request{ method, .. } => {
                match method {
                    Method::Get => {
                        Ok(Message::Response)
                    },
                    _ => {
                        panic!("Not implemented yet.");
                    }
                }
            },
            _ => {
                Err("Only request messages supposed to be processed by Resource.")
            }
        }
    }

}

impl<T: Identifiable> Storage<T> {
    pub fn new() -> Self {
        Storage {
            origin: BTreeMap::new(),
        }
    }

    pub fn insert(&mut self, id: <T as Identifiable>::Id, entity: T) -> Option<T> {
        self.origin.insert(id, entity)
    }

    pub fn values(&self) -> BTreeSet<&T> {
        self.origin.values().collect()
    }

    pub fn get(&self, id: &<T as Identifiable>::Id) -> Option<&T> {
        self.origin.get(id)
    }

    pub fn remove(&mut self, id: &<T as Identifiable>::Id) -> Option<T> {
        self.origin.remove(id)
    }

    pub fn len(&self) -> usize {
        self.origin.len()
    }
}

impl<T: Identifiable> Serialize for Link<T> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut state = serializer.serialize_struct("Resources", 3)?;
        state.serialize_field("href", &format!("/{}/{}", T::identity(), &self.id))?;
        state.end()
    }
}

impl<'de, T: Identifiable> Deserialize<'de> for Link<T> {
        fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where D: Deserializer<'de> {
            enum Field { Href }
            const FIELDS: &'static [&'static str] = &["href"];

            impl<'de> Deserialize<'de> for Field {
                    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
                where
                    D: Deserializer<'de> {

                struct FieldVisitor;

                impl<'de> Visitor<'de> for FieldVisitor {
                    type Value = Field;
                    
                    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                        formatter.write_str("`href`")
                    }

                    fn visit_str<E>(self, value: &str) -> Result<Field, E>
                    where
                        E: de::Error,
                    {
                        match value {
                            "href" => Ok(Field::Href),
                            _ => Err(de::Error::unknown_field(value, FIELDS)),
                        }
                    }
                }
                deserializer.deserialize_identifier(FieldVisitor)
            }
        }

        struct LinkVisitor<T: Identifiable> {
            phantom: PhantomData<T> 
        }

        impl<'de, T: Identifiable> Visitor<'de> for LinkVisitor<T> {
            type Value = Link<T>;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("struct Link")
            }

            fn visit_seq<V>(self, mut seq: V) -> Result<Link<T>, V::Error>
            where
                V: SeqAccess<'de>,
            {
                let href: &str = seq.next_element()?
                    .ok_or_else(|| de::Error::invalid_length(0, &self))?;
                let id: &str = href.strip_prefix(&format!("/{}/", T::identity())).ok_or_else(|| de::Error::invalid_value(Unexpected::Other(href), &self))?;
                let id: <T as Identifiable>::Id = id.parse().map_err(|_| de::Error::invalid_value(Unexpected::Other(id), &self))?;
                Ok(Link::<T>::new(id))
            }

            fn visit_map<V>(self, mut map: V) -> Result<Link<T>, V::Error>
            where
                V: MapAccess<'de>,
            {
                let mut href = None;
                while let Some(key) = map.next_key()? {
                    match key {
                        Field::Href => {
                            if href.is_some() {
                                return Err(de::Error::duplicate_field("href"));
                            }
                            href = Some(map.next_value()?);
                        }
                    }
                }
                let href: &str = href.ok_or_else(|| de::Error::missing_field("href"))?;
                let id: &str = href.strip_prefix(&format!("/{}/", T::identity())).ok_or_else(|| de::Error::invalid_value(Unexpected::Other(href), &self))?;
                let id: <T as Identifiable>::Id = id.parse().map_err(|_| de::Error::invalid_value(Unexpected::Other(id), &self))?;
                Ok(Link::<T>::new(id))
            }
        }
        deserializer.deserialize_struct("Link", FIELDS, LinkVisitor{ phantom: PhantomData})
    }
}

impl<T: Identifiable> Serialize for Resource<T> {
    //TODO: make resources free of IO
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        //TODO: maybe serialize as map or anonymous struct?
        let mut state = serializer.serialize_struct("Resources", 3)?;
        //TODO: wrap it into `_links`
        state.serialize_field("self", &format!("/{}", T::identity()))?;
        let mut curie = BTreeMap::new();
        let path = format!("127.0.0.1:8080/{}/{{rel}}", T::identity());
        curie.insert("name", "lh");
        curie.insert("href", &path);
        curie.insert("templated", "true");
        state.serialize_field("curie", &curie)?;
        state.serialize_field("_embedded", &self.storage.values())?;
        state.end()
    }
}
